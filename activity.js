db.fruits.insertMany([
			{
				"name":"Banana",
				"supplier": "Farmer Fruits Inc.",
				"stocks":30,
				"price":20,
				"onSale":true
			},
			{
				"name":"Mango",
				"supplier": "Mango Magic Inc.",
				"stocks":50,
				"price":70,
				"onSale":true
			},
			{
				"name":"Dragon Fruit",
				"supplier": "Farmer Fruits Inc.",
				"stocks":10,
				"price":60,
				"onSale":true
			},
			{
				"name":"Grapes",
				"supplier": "Fruity Co.",
				"stocks":30,
				"price":100,
				"onSale":true
			},
			{
				"name":"Apple",
				"supplier": "Apple Valley",
				"stocks":0,
				"price":20,
				"onSale":false
			},
			{
				"name":"Papaya",
				"supplier": "Fruity Co.",
				"stocks":15,
				"price":60,
				"onSale":true
			}

	])

//2 - fruits on sale

db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$count:"fruits on sale"}

	])

//3

db.fruits.aggregate([
			{$match:{"stocks":{$gt:20}}},
			{$count:"enough stocks"}
	])

//4

db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id:"$supplier", average: {$avg:"$price"}}}
	])


//5 - max price per supplier

db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id:"$supplier", max_price: {$max:"$price"}}}
	])

//6 - min price per supplier
db.fruits.aggregate([
			{$match:{"onSale":true}},
			{$group: {_id:"$supplier", mmin_price: {$min:"$price"}}}
	])